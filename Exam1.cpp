//============================================================================
// Name        : Exam1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


/*
 * exam question 1
 *
circle.o: circle.cpp circle.h
	g++ -Wall -ansi -pedantic -g -O2 -c circle.cpp

square.o: square.cpp square.h
	g++ -Wall -ansi -pedantic -g -O2 -c square.cpp

main.o: main.cpp circle.h square.h
	g++ -Wall -ansi -pedantic -g -O2 -c main.cpp

shapes: main.o foo.o bar.o
    g++ -Wall -ansi -pedantic -g -02 -o shapes main.o circle.o square.o

*/

struct node {
	int data;
	struct node* next;
};


void Push(struct node** headRef, int dataValue){
	struct node* newNode = new struct node;
	newNode->data = dataValue;
	newNode->next = *headRef;
	*headRef = newNode;
}

void AddToEnd(struct node** headRef, int dataValue){
	struct node* current = *headRef;

	if(current == NULL){
		Push(headRef, dataValue);
	}else{
		while(current->next != NULL){
			current = current->next;
		}
		Push(&(current->next), dataValue);
	}
}

void Print(struct node* head){
	struct node* current = head;
	while(current != NULL){
		cout<< current->data << "\n";
		current = current->next;
	}
}

int Length(struct node* head){
	struct node* current = head;
	int counter = 0;
	while(current != NULL){
		counter++;
		current = current->next;
	}
	return counter;
}

/*
 * exam question 2
 */
void copy(struct node *origin, struct node **copy)
{
    if (origin == NULL)
    {
        *copy = NULL;
    }
    else
    {
        node *temp = NULL;

        while (origin != NULL)
        {
            node* newNode = new node;
            newNode->data = origin->data;
            newNode->next = NULL;
            if (temp == NULL)
            {
                temp = newNode;
                *copy = temp;
            }
            else
            {
                temp->next = newNode;
                temp = temp->next;
            }
            origin = origin->next;
        }
    }
    cout << "The address of the head node \n" << copy << endl;
}

/*
 * exam question 3
 */

void removeDuplicates(struct node *del)
{
    struct node *temp;

    while (del != NULL)
    {
    	temp = del;

        while (temp->next != NULL)
        {
            if (del->data == temp->next->data)
            {
                temp->next = temp->next->next;
            }
            else
            {
                temp = temp->next;
            }
        }

        del = del->next;
    }

    cout << "List after remove duplicates \n" << endl;
}

/*
 * exam question 4
 */

void nthLast(struct node* head, int n)
{
	struct node* pointer1 = head;
    struct node* pointer2 = head;

    for (int j = 0; j < n-1 ;j++)
    {
        pointer1 = pointer1->next;
    }

    while(pointer1->next != NULL)
    {
	    pointer1 = pointer1->next;
	    pointer2 = pointer2->next;
    }

    cout << "The address of kth last element \n"<< pointer2 << endl;
}

int main() {
	struct node* head = NULL;
	Push(&head, 10);
	Push(&head, 20);
	Push(&head, 30);
	Push(&head, 60);
	Push(&head, 40);
	Push(&head, 30);
	AddToEnd(&head, 20);
	cout << "The original linked list" << endl;
	Print(head);

	struct node *duplicate = NULL;
	copy(head, &duplicate);
//	Print(duplicate);

	nthLast(head, 5);

	removeDuplicates(head);
	Print(head);

	nthLast(head, 5);
	return 0;
}
